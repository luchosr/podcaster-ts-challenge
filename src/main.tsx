import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { A_DAY_IN_MILLISECONDS } from './utils/podcasterConstants.ts'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: { staleTime: A_DAY_IN_MILLISECONDS, gcTime: 1000 * 60 * 60 * 25 },
  },
})

// eslint-disable-next-line @typescript-eslint/non-nullable-type-assertion-style
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <QueryClientProvider client={queryClient}>
    <App />
  </QueryClientProvider>
)

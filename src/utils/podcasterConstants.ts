export const HEADING_TITLE: string = 'Podcaster'
export const INPUT_SEARCH_PLACEHOLDER: string = 'Filter podcasts...'
export const PODCAST_CARD_SUBHEADING: string = 'Author:'
export const PODCAST_AUTHOR_SUBHEADING_ARTICLE: string = 'by'
export const PODCAST_DESCRIPTION_SUBHEADING: string = 'Description'
export const PODCAST_LIST_HEADING: string = 'Episodes:'
export const A_DAY_IN_MILLISECONDS: number = 86400000
export const ERROR_MESSAGE: string = 'Sorry but an error has occurred: '

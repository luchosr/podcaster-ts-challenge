export const processEscapedTextForReact = (text: string): string => {
  const textWithSpaces = text.replace(/&nbsp;/g, ' ')

  const urlRegex =
    /(https?:\/\/[^\s]+)|(\b(?:https?:\/\/)?(?:www\.)?\S+\.(?:net|com|fm|org|COM|link)\/?\S*\b)/g
  const textWithLinks = textWithSpaces.replace(urlRegex, (url) => {
    if (url.startsWith('http')) {
      return `<a href="${url}" target="_blank" rel="noopener noreferrer">${url}</a>`
    } else {
      return `<a href="http://${url}" target="_blank" rel="noopener noreferrer">${url}</a>`
    }
  })

  return textWithLinks
}

export const transformDate = (dateString: string): string => {
  const date = new Date(dateString)

  const day = date.getDate().toString()
  const month = (date.getMonth() + 1).toString()
  const year = date.getFullYear().toString()

  return `${day}/${month}/${year}`
}

export const millisecondsToHourMinutesAndSeconds = (
  milliseconds: number
): string => {
  const totalSeconds = Math.floor(milliseconds / 1000)
  const hours = Math.floor(totalSeconds / 3600)
  const minutes = Math.floor((totalSeconds % 3600) / 60)
  const seconds = totalSeconds % 60

  const hoursFormatted = hours.toString()
  const minutesFormatted = minutes.toString()
  const secondsFormatted = seconds.toString()

  return `${hoursFormatted}:${minutesFormatted}:${secondsFormatted}`
}

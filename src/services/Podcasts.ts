import { PodcastTrackInfoInterface, PodcastInfoInterface } from '../types'

export const first100PodcastsBaseUrl: string = `https://api.allorigins.win/get?url=${encodeURIComponent(
  'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json'
)}`

export const getFirst100Podcasts = () =>
  fetch(first100PodcastsBaseUrl)
    .then((res) => res.json())
    .then((res) => JSON.parse(res.contents))

export const getSpecificPodcastInfo = (
  podcastId: string | undefined,
  formatterFn: (data: PodcastInfoInterface[]) => PodcastTrackInfoInterface[]
) =>
  fetch(
    `https://api.allorigins.win/get?url=${encodeURIComponent(
      `https://itunes.apple.com/lookup?id=${podcastId}&media=podcast&entity=podcastEpisode&limit=20`
    )}`
  )
    .then((res) => res.json())
    .then((res) => formatterFn(JSON.parse(res.contents).results))

import { useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import DataTable from 'react-data-table-component'

import {
  PodcastTrackInfoInterface,
  PodcastInfoInterface,
  formatEpisodesListDataInterface,
} from '../../types'
import { useLocalStorage } from '../../hooks/useLocalStorage'
import {
  millisecondsToHourMinutesAndSeconds,
  transformDate,
} from '../../utils/podcasterUtils'
import { PODCAST_LIST_HEADING } from '../../utils/podcasterConstants'

import './PodcastEpisodes.styles.css'
import { usePodcastsEpisodesQuery } from '../../hooks/usePodcastEpisodesQuery'

export const PodcastEpisodes: React.FC = () => {
  const params = useParams()

  const [, setFormattedData] = useState<PodcastTrackInfoInterface[]>([])

  const [, setPodcastInfo] = useLocalStorage(`${params.id}`, '')

  const formatEpisodesListData: formatEpisodesListDataInterface = (
    podcastData: PodcastInfoInterface[]
  ): PodcastTrackInfoInterface[] => {
    setPodcastInfo(podcastData)
    const podcastFormattedData = podcastData
      .slice(1)
      .map((podcast: PodcastInfoInterface, i: number) => ({
        id: i + 1,
        title: podcast.trackName,
        date: transformDate(podcast.releaseDate),
        duration: millisecondsToHourMinutesAndSeconds(podcast.trackTimeMillis),
        trackId: podcast.trackId,
        collectionId: podcast.collectionId,
      }))
    setFormattedData(podcastFormattedData)
    return podcastFormattedData
  }

  const { data, error } = usePodcastsEpisodesQuery(
    params.id,
    formatEpisodesListData
  )

  const columns = [
    {
      name: 'Title',
      width: '60%',
      selector: (row: PodcastTrackInfoInterface): React.JSX.Element => (
        <Link
          className="table__row"
          to={`/podcasts/${row.collectionId}/episode/${row.trackId}`}
        >
          {row.title}
        </Link>
      ),
    },
    {
      name: 'Date',
      selector: (row: PodcastTrackInfoInterface): string => row.date,
    },
    {
      name: 'Duration',
      selector: (row: PodcastTrackInfoInterface): string | undefined =>
        row.duration,
    },
  ]

  const conditionalRowStyles = [
    {
      when: (row: PodcastTrackInfoInterface): boolean => row.id % 2 > 0,
      style: {
        backgroundColor: 'var( --row-white)',
        textDecoration: 'none',
      },
    },
  ]

  if (error) return 'Sorry but an error has occurred: ' + error.message

  return (
    <>
      {data ? (
        <div className="podcastEpisodesWrapper">
          <div className="podcastEpisodes__titleCounter">
            <h2
              data-testid="podcasts-episodes-number"
              className="heading__title"
            >
              {PODCAST_LIST_HEADING} {data?.length}
            </h2>
          </div>
          <div
            data-testid="podcasts-track-table"
            className="podcastEpisodes__tableWrapper "
          >
            <DataTable
              // @ts-expect-error: typescript exception
              columns={columns}
              data={data!}
              conditionalRowStyles={conditionalRowStyles}
            />
          </div>
        </div>
      ) : (
        ''
      )}
    </>
  )
}

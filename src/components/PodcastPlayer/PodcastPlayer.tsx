import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import { useLocalStorage } from '../../hooks/useLocalStorage'

import { processEscapedTextForReact } from '../../utils/podcasterUtils'

import './PodcastPlayer.styles.css'
import { PodcastInfoInterface } from '../../types'

export const PodcastPlayer: React.FC = () => {
  const [trackInfo, setTrackInfo] = useState<PodcastInfoInterface[]>([])
  const params = useParams()

  const [podcastList] = useLocalStorage(`${params.id}`, '')

  useEffect(() => {
    const podcastFormattedData = podcastList
      .slice(1)
      .filter(
        (podcast: PodcastInfoInterface) =>
          podcast.trackId === Number(params.episodeId)
      )
    setTrackInfo(podcastFormattedData)
  }, [])

  return (
    <div data-testid="podcast-track" className="podcastPlayerWrapper">
      <h2 className="podcastPlayerWrapper__title">{trackInfo[0]?.trackName}</h2>
      <div
        className="podcastPlayerWrapper__description"
        dangerouslySetInnerHTML={{
          __html: processEscapedTextForReact(`${trackInfo[0]?.description}`),
        }}
      />
      <audio
        className="podcastPlayerWrapper__audioControl"
        src={trackInfo[0]?.episodeUrl}
        controls
      />
    </div>
  )
}

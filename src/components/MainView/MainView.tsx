import { useState } from 'react'

import { PodcastInterface } from '../../types'
import { usePodcastsListQuery } from '../../hooks/useGetPodcastsListQuery'
import {
  ERROR_MESSAGE,
  INPUT_SEARCH_PLACEHOLDER,
} from '../../utils/podcasterConstants'

import { Card } from '../Card/Card'

import './MainView.styles.css'

export const MainView: React.FC = () => {
  const [filterValue, setFilterValue] = useState<string>('')

  const { data, error } = usePodcastsListQuery()

  const filteredPodcasts: (PodcastInterface | undefined)[] | undefined =
    data?.feed?.entry?.filter(
      (entry: PodcastInterface | undefined): entry is PodcastInterface => {
        if (!entry) return false
        const artistLabel = entry['im:artist'].label.toLowerCase()
        const nameLabel = entry['im:name'].label.toLowerCase()
        const filter = filterValue.toLowerCase()
        return artistLabel.includes(filter) || nameLabel.includes(filter)
      }
    )

  if (error) return `${ERROR_MESSAGE}` + error.message

  return (
    <>
      {data ? (
        <div className="inputWrapper">
          <span data-testid="podcasts-number" className="inputWrapper__counter">
            {filteredPodcasts?.length}
          </span>
          <input
            data-testid="input-search"
            name="podcastSearch"
            className="inputWrapper__input"
            type="text"
            value={filterValue}
            placeholder={INPUT_SEARCH_PLACEHOLDER}
            onChange={(event) => setFilterValue(event.target.value)}
          />
        </div>
      ) : (
        ''
      )}

      <div className="cardGrid">
        {data
          ? filteredPodcasts?.map(
              (entry: PodcastInterface | undefined, index: number) => (
                <Card
                  index={index}
                  podcastId={entry!.id.attributes['im:id']}
                  key={entry!.id.attributes['im:id']}
                  podcastAuthor={entry!['im:artist'].label}
                  podcastName={entry!['im:name'].label}
                  podcastDescription={entry!.summary.label}
                  imgUrl={entry!['im:image'][2].label}
                />
              )
            )
          : ''}
      </div>
    </>
  )
}

import { Link } from 'react-router-dom'
import { CardInterface } from '../../types'
import { useLocalStorage } from '../../hooks/useLocalStorage'

import './Card.styles.css'
import { PODCAST_CARD_SUBHEADING } from '../../utils/podcasterConstants'

export const Card: React.FC<CardInterface> = ({
  imgUrl,
  podcastName,
  podcastAuthor,
  podcastId,
  podcastDescription,
  index,
}) => {
  const [podcastCardInfo, setPodcastCardInfo] = useLocalStorage(
    'podcastCardInfo',
    ''
  )

  return (
    <Link
      className="cardWrapper"
      data-testid={`card-link-${index}`}
      to={`/podcasts/${podcastId}`}
      onClick={() =>
        setPodcastCardInfo({
          author: podcastAuthor,
          name: podcastName,
          description: podcastDescription,
          image: imgUrl,
        })
      }
    >
      <div className="cardWrapper__imageContainer">
        <img
          src={imgUrl}
          alt={podcastCardInfo.author}
          className="imageContainer__roundImage"
        />
      </div>
      <div className="cardWrapper__content">
        <h2 data-testid="card-heading" className="content__title">
          {podcastName}
        </h2>
        <p data-testid="card-description" className="content__subTitle">
          {PODCAST_CARD_SUBHEADING} {podcastAuthor}
        </p>
      </div>
    </Link>
  )
}

import { Link, useParams } from 'react-router-dom'
import { useIsFetching } from '@tanstack/react-query'
import ScaleLoader from 'react-spinners/ScaleLoader'

import { HEADING_TITLE } from '../../utils/podcasterConstants'
import './PodcasterHeading.styles.css'

export const PodcasterHeading: React.FC = () => {
  const params = useParams()
  const isFetchingPodcasts = useIsFetching({
    queryKey: ['first100Podcasts'],
  })

  const isFetchingPodcastEpisodes = useIsFetching({
    queryKey: [`${params.id}`],
  })

  return (
    <>
      <div className="headingWrapper">
        <h1 data-testid="heading-title" className="headingWrapper__title">
          <Link className="headingWrapper__link" to="/">
            {HEADING_TITLE}
          </Link>
        </h1>
        <div className="headingWrapper__loader">
          {isFetchingPodcasts > 0 || isFetchingPodcastEpisodes > 0 ? (
            <ScaleLoader
              data-testid="heading-loader"
              color="var(--loader)"
              width={15}
              height={40}
            />
          ) : (
            ''
          )}
        </div>
      </div>
    </>
  )
}

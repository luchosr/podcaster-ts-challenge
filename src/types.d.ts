export interface RouteError {
  data: string
  message: string
  statusText: string
}

export interface CardInterface {
  imgUrl: string
  podcastName: string
  podcastAuthor: string
  podcastId: string
  podcastDescription: string
  index: number
}

export interface PodcastInterface {
  'im:name': {
    label: string
  }
  'im:image': {
    label: string
    attributes: {
      height: string
    }
  }[]
  summary: {
    label: string
  }
  'im:price': {
    label: string
    attributes: {
      amount: string
      currency: string
    }
  }
  'im:contentType': {
    attributes: {
      term: string
      label: string
    }
  }
  rights: {
    label: string
  }
  title: {
    label: string
  }
  link: {
    attributes: {
      rel: string
      type: string
      href: string
    }
  }
  id: {
    label: string
    attributes: {
      'im:id': string
    }
  }
  'im:artist': {
    label: string
  }
  category: {
    attributes: {
      'im:id': string
      term: string
      scheme: string
      label: string
    }
  }
  'im:releaseDate': {
    label: string
    attributes: {
      label: string
    }
  }
}

export interface PodcastInfoInterface {
  wrapperType: string
  kind: string
  collectionId: number
  trackId: number
  artistName: string
  collectionName: string
  trackName: string
  collectionCensoredName: string
  trackCensoredName: string
  collectionViewUrl: string
  feedUrl: string
  trackViewUrl: string
  artworkUrl30: string
  artworkUrl60: string
  artworkUrl100: string
  collectionPrice: number
  trackPrice: number
  collectionHdPrice: number
  releaseDate: string
  collectionExplicitness: string
  trackExplicitness: string
  trackCount: number
  episodeUrl?: string
  description?: string
  trackTimeMillis: number
  country: string
  currency: string
  primaryGenreName: string
  contentAdvisoryRating: string
  artworkUrl600: string
  genreIds: string[]
  genres: string[]
}

export interface PodcastTrackInfoInterface {
  id: number
  title: string
  date: string
  trackId: number
  duration?: string
  collectionId: number
}

export interface PodcastCardInfoInterface {
  author: string
  name: string
  description: string
  image: string
}

export interface formatEpisodesListDataInterface {
  (podcastData: PodcastInfoInterface[]): PodcastTrackInfoInterface[]
}

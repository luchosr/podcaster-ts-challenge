import { useQuery } from '@tanstack/react-query'
import { getFirst100Podcasts } from '../services/Podcasts'
import { A_DAY_IN_MILLISECONDS } from '../utils/podcasterConstants'

export const usePodcastsListQuery = () => {
  const { data, error } = useQuery({
    queryKey: ['first100Podcasts'],
    queryFn: getFirst100Podcasts,
    refetchInterval: A_DAY_IN_MILLISECONDS,
    refetchOnWindowFocus: false,
    retry: 3,
  })

  return { data, error }
}

import { useState } from 'react'

export const useLocalStorage = (key: string, initialValue: object | string) => {
  const getStoredValue = () => {
    try {
      const item = window.localStorage.getItem(key)

      return item ? JSON.parse(item) : { value: initialValue }
    } catch (error) {
      return { value: initialValue }
    }
  }

  const [storedValue, setStoredValue] = useState(getStoredValue)

  const setValue = (value: object | string) => {
    try {
      const updatedValue = {
        value,
      }

      setStoredValue(updatedValue)

      window.localStorage.setItem(key, JSON.stringify(updatedValue))
    } catch (error) {
      console.error(
        'Local Storage could not been updated, the error is: ',
        error
      )
    }
  }

  return [storedValue.value, setValue]
}

import { useQuery } from '@tanstack/react-query'
import { getSpecificPodcastInfo } from '../services/Podcasts'
import { A_DAY_IN_MILLISECONDS } from '../utils/podcasterConstants'
import { formatEpisodesListDataInterface } from '../types'

export const usePodcastsEpisodesQuery = (
  podcastId: string | undefined,
  formatFunction: formatEpisodesListDataInterface
) => {
  const { data, error } = useQuery({
    queryKey: [`${podcastId}`],

    queryFn: () => getSpecificPodcastInfo(podcastId, formatFunction),

    refetchInterval: A_DAY_IN_MILLISECONDS,
    refetchOnWindowFocus: false,
    retry: 3,
  })

  return { data, error }
}

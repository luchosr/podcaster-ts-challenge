import { useRouteError } from 'react-router-dom'
import { RouteError } from '../types'

export const ErrorPage: React.FC = () => {
  const error = useRouteError() as RouteError
  console.error(error)

  return (
    <div id="error-page">
      <h1>Uh! oh!</h1>
      <p>Sorry but, an unexpected error has occurred.</p>
      <p>
        <i>{error.statusText || error.message}</i>
      </p>
    </div>
  )
}

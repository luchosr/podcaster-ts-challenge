import { Outlet } from 'react-router-dom'

import { Podcast } from '../../components/Podcast/Podcast'

import './PodcastRoot.styles.css'

export const PodcastRoot: React.FC = () => {
  return (
    <>
      <div className="podcastRootWrapper">
        <Podcast />
        <Outlet />
      </div>
    </>
  )
}

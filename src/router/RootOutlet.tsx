import { Outlet } from 'react-router-dom'

import { PodcasterHeading } from '../components/PodcasterHeading/PodcasterHeading'

export const RootOutlet: React.FC = () => {
  return (
    <>
      <PodcasterHeading />
      <Outlet />
    </>
  )
}

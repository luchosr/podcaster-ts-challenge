describe('Main view page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:5173/')
  })

  context('Heading section', () => {
    it('renders a correct heading', () => {
      cy.get('[data-testid="heading-title"]')
        .should('exist')
        .should('have.text', 'Podcaster')
    })

    it('renders a loading component', () => {
      cy.get('[data-testid="heading-loader"]').should('exist')
    })
  })

  context('Grid section', () => {
    beforeEach(() => {
      cy.wait(20000)
    })

    it('renders a correct input', () => {
      cy.get('[data-testid="input-search"]').should('exist')
    })

    it('renders a podcasts number', () => {
      cy.get('[data-testid="podcasts-number"]').should('exist')
    })

    it('renders a card component correctly', () => {
      cy.get('[data-testid="card-link"]').find('img')
      cy.get('[data-testid="card-heading"]').should('exist')
      cy.get('[data-testid="card-description"]').should('exist')
    })
  })
})

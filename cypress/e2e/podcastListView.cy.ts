describe('Podcast List page', () => {
  context('testing heading behavior', () => {
    beforeEach(() => {
      cy.visit('http://localhost:5173/')
      cy.wait(20000)
      cy.get('[data-testid="card-link-0"]').should('exist').click()
    })

    it('renders a correct heading', () => {
      cy.get('[data-testid="heading-title"]')
        .should('exist')
        .should('have.text', 'Podcaster')
    })

    it('renders a loading component', () => {
      cy.get('[data-testid="heading-loader"]').should('exist')
    })
  })

  context('testing card navigation', () => {
    beforeEach(() => {
      cy.visit('http://localhost:5173/')
      cy.wait(20000)
    })

    it('navigates clicking a card', () => {
      cy.get('[data-testid="card-link-0"]').should('exist').click()
    })

    it.only('needs to render a correct podcast information', () => {
      cy.get('[data-testid="card-link-0"]').should('exist').click()
      cy.get('[data-testid="podcast-card"]').find('img').should('exist')
      cy.get('[data-testid="podcast-card"]').find('h2').should('exist')
      cy.get('[data-testid="podcast-card"]').find('p').should('exist')
      cy.get('[data-testid="podcast-card"]').find('h4').contains('Description')
    })

    it('needs to show an amount of episodes', () => {
      cy.get('[data-testid="card-link-0"]').should('exist').click()
      cy.wait(7000)
      cy.get('[data-testid="podcasts-episodes-number"]').should('exist')
    })

    it('needs to show a table of tracks', () => {
      cy.get('[data-testid="card-link-0"]').should('exist').click()
      cy.wait(7000)
      cy.get('[data-testid="podcasts-track-table"]').should('exist')
      cy.get('a[class="table__row"]').eq(0).click()
    })
  })
})

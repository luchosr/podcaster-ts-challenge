describe('Track List page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:5173/')
    cy.wait(20000)
    cy.get('[data-testid="card-link-0"]').should('exist').click()
    cy.wait(9000)
    cy.get('a[class="table__row"]').eq(0).click()
  })

  it('renders a  podcast information', () => {
    cy.get('[data-testid="podcast-card"]').find('img')
    cy.get('[data-testid="podcast-card"]').find('h2')
    cy.get('[data-testid="podcast-card"]').find('p')
    cy.get('[data-testid="podcast-card"]').find('h4').contains('Description')
  })

  context('podcast track section', () => {
    it('renders a track heading', () => {
      cy.get('[data-testid="podcast-track"]').find('h2')
    })

    it('renders a track description', () => {
      cy.get('[data-testid="podcast-track"]')
        .find('div')
        .should('have.class', 'podcastPlayerWrapper__description')
    })

    it('displays an audio player', () => {
      cy.get('[data-testid="podcast-track"]')
        .find('audio')
        .should('have.class', 'podcastPlayerWrapper__audioControl')
    })
  })
})
